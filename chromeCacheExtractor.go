/*

########### DESCRIPTION ###########

Parse Chrome cache files from a Ubuntu Linux system

Reference: https://github.com/chromium/chromium/blob/2ca8c5037021c9d2ecc00b787d58a31ed8fc8bcb/net/disk_cache/simple/simple_entry_format_history.h


########### PRE-REQS ###########
install git client
get needed libraries, if needed, like this...
	go get -u golang.org/x/text/encoding/unicode   # not need, just an example

########### TO RUN ###########
testing: go run file.go

########### TO BUILD ###########
if compiling for 32-bit...
	set GOARCH=386
if compiling for 64-bit...
	set GOARCH=amd64

go build -ldflags="-s -w" file.go [-o outputFileName]

########### TO DO ###########

- allow numberOfHoursToLookBackIntoCache to be "-1" and search all

- fix decode issue with url encoded values (ex %2F)
- detect gzip compression (common for web) and decompress in memory
- determine best extension based on content type
- make more user friendly, to include command line parameters (instead of hard-coded values), auto discovery of cache location based on common paths, etc.
- test on other OSs and make mods as appropriate
- command param to write to CSV
- maybe create a new (sub) folder each run to place the cached files to ensure nothing is overwritten. Make sure folder creation does not overwrite.
- Parse other data after stream 1 (and FINAL_MAGIC_NUMBER). ex. raw HTTP response would be very useful
- compute and verify key/URL and file hashes
- Look into this: most cache files (as of today, on ubuntu linux) end with "_0" and are version 5. There are a few that end with "_s" and are version 9.

###############################

*/

package main


import (
    "fmt"
    "io/ioutil"
    "encoding/hex"
    "os"
    "encoding/binary"
    "bytes"
    "time"
    "sort"
    "net/http"
    //"net/url"

)


const (
    INITIAL_MAGIC_NUMBER = "305c72a71b6dfbfc"  // 64-bit little endian string representation of 0xfcfb6d1ba7725c30
    VERSION = "05000000"  // 32-bit little endian string representation
    PAD_32 = "00000000"
    FINAL_MAGIC_NUMBER = "d8410d97456ffaf4"  // 64-bit little endian string representation of 0xf4fa6f45970d41d8
    FILE_PERMISSIONS = 0644

)

var (  //global vars
    pathToCacheFiles = "/home/user/.cache/google-chrome/Default/Cache/"
    pathToOutputFiles = "/tmp/"   // a subfolder will be created under this path yyy-mm-dd_hh-mm-ss
    numberOfHoursToLookBackIntoCache = 1  // for now, must be > 0.  FUTURE: -1 for all
)



func debugprint(myStr string){
    return
	fmt.Printf(myStr)
	fmt.Printf("\n")
}

func main() {

    debugprint(fmt.Sprintf("starting...\n"))

    createOutputDirectory()

    // glob files in cache folder (files ending with _0)
    // Source: https://stackoverflow.com/questions/44380054/how-to-iterate-through-directory-ordered-based-on-the-file-time
    f, _ := os.Open(pathToCacheFiles)
    fis, _ := f.Readdir(-1)
    f.Close()
    sort.Sort(ByModTime(fis))
    for _, fi := range fis {
        if time.Now().Sub(fi.ModTime()) > time.Duration(numberOfHoursToLookBackIntoCache) * time.Hour {
            continue
        }
        fileName := fi.Name()
        if fileName[len(fileName)-2:] != "_0" {
            continue
        }

        processCacheFile(fileName)

    }

}


func createOutputDirectory(){

    t := time.Now()
    currentTimeString := fmt.Sprintf("%d-%02d-%02d_%02d-%02d-%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
    pathToOutputFiles += currentTimeString + "/"

    _, err := os.Stat(pathToOutputFiles)
    if os.IsNotExist(err) {
        errDir := os.MkdirAll(pathToOutputFiles, 0755)
        if errDir != nil {
            fmt.Printf("Cannot create output directory \"%v\". Error: %v", pathToOutputFiles, err)
            os.Exit(0)
        }
    }

}


// SOURCE: https://stackoverflow.com/questions/44380054/how-to-iterate-through-directory-ordered-based-on-the-file-time
type ByModTime []os.FileInfo
func (fis ByModTime) Len() int {
    return len(fis)
}
func (fis ByModTime) Swap(i, j int) {
    fis[i], fis[j] = fis[j], fis[i]
}
func (fis ByModTime) Less(i, j int) bool {
    return fis[i].ModTime().Before(fis[j].ModTime())
}



func processCacheFile(fileName string)(){

    debugprint(fmt.Sprintf("\nfileName: %s", fileName))

    filePath := pathToCacheFiles + fileName

    file, err := os.Stat(filePath)
    if err != nil {
        fmt.Printf("Cannot open file \"%v\" to stat it. Error: %v", filePath, err)
        os.Exit(0)
    }
    fileModTime := file.ModTime()
    debugprint(fmt.Sprintf("Last modified time: %s", fileModTime))

    // read contents of entire cache file into byte array
    binaryData, err := ioutil.ReadFile(filePath)
    if err != nil {
        fmt.Printf("Cannot open file \"%v\" to read it. Error: %v", filePath, err)
        os.Exit(0)
    }
    
    if fmt.Sprintf("%x", binaryData[0:8]) != INITIAL_MAGIC_NUMBER {
        fmt.Printf("Fail: INITIAL_MAGIC_NUMBER")
        os.Exit(0)
    }

    debugprint("Pass: INITIAL_MAGIC_NUMBER")

    if fmt.Sprintf("%x", binaryData[8:12]) != VERSION {
        fmt.Printf("Fail: VERSION")
        os.Exit(0)
    }

    debugprint("Pass: VERSION")

    key_length := binary.LittleEndian.Uint32(binaryData[12:16])
    debugprint(fmt.Sprintf("Key/URL length is %v (0x%x - 32-bit little endian)", key_length, binaryData[12:16]))

    //key_hash := binaryData[16:20]   // 32-bit little endian
    // maybe use this in the future to verify URL/key
    
    if fmt.Sprintf("%x", binaryData[20:24]) != PAD_32 {
        fmt.Printf("Fail: PAD_32")
        os.Exit(0)
    }

    debugprint("Pass: PAD_32")

    startOfStream1Data := 24 + key_length
    URL := binaryData[24:startOfStream1Data]
    //test1, test2 := fmt.Printf("%s", string(URL))
    //test3 := fmt.Sprintf("%#s", URL)
    //fmt.Printf(test3)
    debugprint(fmt.Sprintf("URL: %s", URL))
    //debugprint("URL: " + url.QueryEscape(URL))

    // find offest to FINAL_MAGIC_NUMBER
    finalMagicNumber_byteArray, err := hex.DecodeString(FINAL_MAGIC_NUMBER)
    if err != nil {
        fmt.Printf("Cannot convert FINAL_MAGIC_NUMBER. Error: %v", err)
        os.Exit(0)
    }
    
    stream1Length := bytes.Index(binaryData[startOfStream1Data:], finalMagicNumber_byteArray)
    if stream1Length < 0 {
        fmt.Printf("Fail: FINAL_MAGIC_NUMBER (file: " + fileName + ")")
        os.Exit(0)
    }
    startOfFinalMagicNumber := int(startOfStream1Data) + stream1Length
    debugprint(fmt.Sprintf("startOfStream1Data: %d, startOfFinalMagicNumber: %d, stream1Length: %d", startOfStream1Data, startOfFinalMagicNumber, stream1Length))
    
    // PROBABLY HERE FOR DETECTING AND DECODING GZIP COMPRESSION
    
    // determine best extension to use
    contentType, extBasedOnContentAnalysis := determineExtension(binaryData[startOfStream1Data:startOfStream1Data+20])
    
    // write binary data to disk until FINAL_MAGIC_NUMBER
    err = ioutil.WriteFile(pathToOutputFiles + fileName + extBasedOnContentAnalysis, binaryData[startOfStream1Data:startOfFinalMagicNumber], FILE_PERMISSIONS)
    if err != nil { 
        fmt.Printf("Fail: Writing file to disk")
        os.Exit(0)
    }
    
    fmt.Printf("%s,%s,%s,%s\n", fileName, contentType, URL, fileModTime)
	//fmt.Printf(myStr)


}


func determineExtension(binaryData []byte) (string, string) {

    contentType := http.DetectContentType(binaryData)
    debugprint(fmt.Sprintf("contentType: %v", contentType))

    // NEED TO SELECT APPROPRIATE EXTENSION HERE
    
    return contentType, ".bin"

    
}


/*

    //fmt.Println(binaryData) // print the content as integer representaion of 'bytes'
    //fmt.Printf("%s", hex.Dump(binaryData))
    //fmt.Println(string(binaryData)) // print the content as a 'string'
    //for i := 0; i < len(binaryData); i++ {
    //    //fmt.Println(binaryData[i])
    //    debugprint(fmt.Sprintf("%x", binaryData[i]))
    //}
    //debugprint(fmt.Sprintf("%x", binaryData[0:8]))

*/